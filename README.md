# Grafiker Müşteri Takip Programı

![Alt text](https://github.com/Gelisimsoft/GrafikerMusteriTakipProgrami-CSharp-/raw/master/GrafikerMusteriTakipProgrami.jpg)

## GİRİŞ BİLGİLERİ ##
Kullanıcı adı	: admin 
Şifre		: demo 

## MODÜLLER ##

1. Müşteri Kayıt
2. Matbaa Kayıt
3. İş Tanım Kayıt
4. Durum Kayıt
5. Personel Kayıt
6. İş Emri Kayıt
7. Müşteri Raporlar
8. Matbaa Raporlar
9. İş Tanım Raporlar
10. Durum Raporlar
11. Personel Raporları
12. Kullanıcı İşlemleri


www.gelisimsoft.com 
info@gelisimsoft.com


